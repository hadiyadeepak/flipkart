package com.flipkart.components;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ProductsList extends QAFWebComponent{

	public ProductsList(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}
	
	@FindBy(locator="productpage.lbl.productprice")
	private QAFWebElement productPrice;

	@FindBy(locator="productpage.lbl.productname")
	private QAFWebElement productName;

	public QAFWebElement getProductPrice() {
		return productPrice;
	}

	public QAFWebElement getProductName() {
		return productName;
	}
	

}
