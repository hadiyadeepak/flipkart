package com.flipkart.tests;

import java.util.Map;

import org.testng.annotations.Test;

import com.flipkart.beans.ProductBean;
import com.flipkart.pages.CartPage;
import com.flipkart.pages.FlipkarthomePage;
import com.flipkart.pages.ProductDetailPage;
import com.flipkart.pages.ProductPage;
import com.flipkart.pages.SignupPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class FlipkartSearchProductTest extends WebDriverTestCase {

	@QAFDataProvider(key = "flipkart.product")
	@Test
	public void testSearchProduct(Map<String, String> data) {

		// Launch Flipkart application
		SignupPage signupPage = new SignupPage();
		signupPage.launchApplication();

		// Tap on accept button of access contacts popup
		signupPage.getBtnAllowAccessOfContacts().click();

		// Tap on cancel button of sign-up page
		signupPage.getBtnCancel().click();

		// Tap on Menu (drawer) products
		FlipkarthomePage homePage = new FlipkarthomePage();
		homePage.getHomepageMenuCategorydrawer().click();

		// Select product form the category
		homePage.selectCategory(data.get("category"));

		// Select product Sub Category
		homePage.selectSubCategory(data.get("subcategory"));

		// Tap on search button
		ProductPage productPage = new ProductPage();
		productPage.getBtnSearch().click();

		// Enter the test in the search text box
		productPage.getTboxSearch().sendKeys(data.get("product"));

		// Selecting the first product from the auto suggested list
		productPage.selectProductFromDropdownListOfsearchedProduct(0);

		// Check for device location popup if appears press on allow button
		productPage.selectAllowButtonFromLocationPopup();

		// printing the searched result of the product in the dashboard
		productPage.printProducts();

		// sorting the products by lowest price and selecting first product
		ProductBean searchedProduct = productPage.selectProductByLowestPrice(0);

		// Comparing product form product detail page with product from product page
		ProductDetailPage detailPage = new ProductDetailPage();
		detailPage.isSameProductInDetailPage(searchedProduct);

		// Adding product to the shopping cart
		detailPage.selectAddToCart();

		// Tapping on cart icon
		detailPage.selectIconCart();

		// Waiting for the product till 30 sec to get it visible
		CartPage cartPage = new CartPage();
		cartPage.getLblProductname().waitForVisible();

		// Verifying selected product is in cart
		cartPage.isProductInCart(searchedProduct);

	}

}
