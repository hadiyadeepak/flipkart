package com.flipkart.pages;

import org.hamcrest.Matchers;

import com.flipkart.beans.ProductBean;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class ProductDetailPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "productdetailpage.lbl.productname")
	private QAFWebElement lblProductname;

	@FindBy(locator = "productdetailpage.lbl.productprice")
	private QAFWebElement lblProductprice;

	@FindBy(locator = "productdetailpage.btn.addtocart")
	private QAFWebElement btnAddtocart;

	public QAFWebElement getBtnAddtocart() {
		return btnAddtocart;
	}

	public QAFWebElement getIconCart() {
		return iconCart;
	}

	@FindBy(locator = "productdetailpage.icon.cart")
	private QAFWebElement iconCart;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getLblProductname() {
		return lblProductname;
	}

	public QAFWebElement getLblProductprice() {
		return lblProductprice;
	}

	public String getProductPriceWithoutComma() {
		String price = lblProductprice.getText();
		// removing , from price
		return price.replace(",", "");
	}

	public String getProductNameWithoutColour() {
		String pName = lblProductname.getText();
		int index = pName.indexOf(" (");
		return pName.substring(0, index);
	}

	public void selectAddToCart() {
		btnAddtocart.click();
	}

	public void selectIconCart() {
		iconCart.click();
	}
	

	public void isSameProductInDetailPage(ProductBean product) {
		String productNameInDetailPage = getProductNameWithoutColour();
		String productPriceInDetailPage = getProductPriceWithoutComma();
		Validator.verifyThat(
				"Name = " + productNameInDetailPage + " price = Rs."
						+ productPriceInDetailPage,
				Matchers.equalTo("Name = " + product.getProductName() + " price = Rs."
						+ product.getProductPrice()));

	}
}
