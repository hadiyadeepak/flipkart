package com.flipkart.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class SignupPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "signuppage.btn.allowaccessofcontacts")
	private QAFWebElement btnAllowAccessOfContacts;

	@FindBy(locator = "signuppage.btn.cancel")
	private QAFWebElement btnCancel;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {

	}

	public void launchApplication() {
		openPage(null);
	}

	public QAFWebElement getBtnAllowAccessOfContacts() {
		return btnAllowAccessOfContacts;
	}

	public QAFWebElement getBtnCancel() {
		return btnCancel;
	}

}
