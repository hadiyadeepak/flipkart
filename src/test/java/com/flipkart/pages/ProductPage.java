package com.flipkart.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.flipkart.beans.ProductBean;
import com.flipkart.components.ProductsList;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

import io.appium.java_client.MobileElement;

public class ProductPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "productpage.btn.search")
	private QAFWebElement btnSearch;

	public QAFWebElement getBtnSearch() {
		return btnSearch;
	}

	@FindBy(locator = "productpage.tbox.search")
	private QAFWebElement tboxSearch;

	public QAFWebElement getTboxSearch() {
		return tboxSearch;
	}

	public QAFWebElement getPopupOfLocation() {
		return popupOfLocation;
	}

	public QAFWebElement getBtnAllowOfLocationPopup() {
		return btnAllowOfLocationPopup;
	}

	public List<ProductsList> getProductList() {
		return productList;
	}

	@FindBy(locator = "productpage.btn.sortby")
	private QAFWebElement btnSortBy;

	@FindBy(locator = "productpage.btn.sortbypricelowtohigh")
	private QAFWebElement btnSortByPriceLowToHigh;

	public QAFWebElement getBtnSortBy() {
		return btnSortBy;
	}

	public QAFWebElement getBtnSortByPriceLowToHigh() {
		return btnSortByPriceLowToHigh;
	}

	@FindBy(locator = "productpage.popup.oflocation")
	private QAFWebElement popupOfLocation;

	@FindBy(locator = "productpage.btn.allow")
	private QAFWebElement btnAllowOfLocationPopup;

	@FindBy(locator = "productpage.list.productlist")
	private List<ProductsList> productList;

	@FindBy(locator = "productpage.dropdownlist.searchedproduct")
	private List<QAFWebElement> dropdownListOfsearchedProduct;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public void printProducts() {
		for (ProductsList product : productList) {
			String productName = product.getProductName().getText();
			String productPriceWithRs = product.getProductPrice().getText();
			int indexOfRs = productPriceWithRs.indexOf("Rs.");
			String productPrice =
					productPriceWithRs.substring(indexOfRs + "Rs.".length());
			Reporter.log("<<< Product Name = " + productName
					+ " >>>> <<< Product Price = " + productPrice + " >>>");
		}
	}

	public void selectProductFromDropdownListOfsearchedProduct(int selectByIndex) {
		dropdownListOfsearchedProduct.get(selectByIndex).waitForVisible();
		dropdownListOfsearchedProduct.get(selectByIndex).click();
	}

	public void selectAllowButtonFromLocationPopup() {
		try
		{
			btnAllowOfLocationPopup.waitForVisible();
			btnAllowOfLocationPopup.click();
		}
		catch(Exception e)
		{
			
		}
	}

	public ProductBean selectProductByLowestPrice(int index) {
		btnSortBy.click();
		btnSortByPriceLowToHigh.click();

		ProductsList product = productList.get(index);
		String productName = product.getProductName().getText();
		String productPriceWithRs = product.getProductPrice().getText();
		int indexOfRs = productPriceWithRs.indexOf("Rs.");
		String productPrice = productPriceWithRs.substring(indexOfRs + "Rs.".length());
		product.click();
		return new ProductBean(productName, productPrice);

	}
}
