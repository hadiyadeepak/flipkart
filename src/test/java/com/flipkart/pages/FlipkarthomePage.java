package com.flipkart.pages;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FlipkarthomePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "homepage.menu.categorydrawer")
	private QAFWebElement menuCategorydrawer;
	@FindBy(locator = "homepage.lnk.category")
	private QAFWebElement lnkCategory;
	@FindBy(locator = "homepage.lnk.subcategory")
	private QAFWebElement lnkSubcategory;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getHomepageMenuCategorydrawer() {
		return menuCategorydrawer;
	}

	public void selectCategory(String category)
	{
		QAFWebElement productCategory=new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("homepage.lnk.category"),
				category));
		productCategory.waitForVisible();
		productCategory.click();
	}
	
	public void selectSubCategory(String subCategory)
	{
		QAFWebElement productSubCategory=new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("homepage.lnk.subcategory"),
				subCategory));
		productSubCategory.waitForVisible();
		productSubCategory.click();
	}
}
