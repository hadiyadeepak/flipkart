package com.flipkart.pages;

import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.Reporter;

import com.flipkart.beans.ProductBean;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class CartPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "cartpage.lbl.productname")
	private QAFWebElement lblProductname;
	@FindBy(locator = "cartpage.lbl.productprice")
	private QAFWebElement lblProductprice;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public final QAFWebElement getLblProductname() {
		return lblProductname;
	}

	public final QAFWebElement getLblProductprice() {
		return lblProductprice;
	}

	public String getProductPriceWithoutComma() {
		String price = lblProductprice.getAttribute("contentDescription");
		int beginIndex = price.indexOf("₹");
		int endIndex = price.indexOf("₹", beginIndex + 1);
		return price.substring(beginIndex + 1, endIndex).replace(",", "");
	}

	public void isProductInCart(ProductBean product) {
		String productNameInCartPage =
				getLblProductname().getAttribute("contentDescription");
		String productPriceInCartPage = getProductPriceWithoutComma();
		Validator.verifyThat(
				"Name = " + productNameInCartPage + " price = Rs."
						+ productPriceInCartPage,
				Matchers.equalTo("Name = " + product.getProductName() + " price = Rs."
						+ product.getProductPrice()));

	}

}
