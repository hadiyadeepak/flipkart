package com.flipkart.beans;

import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.Reporter;

import com.qmetry.qaf.automation.util.Validator;

public class ProductBean {

	private String productName;
	private String productPrice;
	public String getProductName() {
		return productName;
	}

	public ProductBean(String productName, String productPrice) {
		this.productName = productName;
		this.productPrice = productPrice;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}

	
}
